
//DECIMAL
#define segA 4
#define segB 5
#define segC 6
#define segD 7
#define segE 8
#define segF 9
#define segG 10

//UNIDADE
#define seg2A 11
#define seg2B 12
#define seg2C 13
#define seg2D A5
#define seg2E A4
#define seg2F A3
#define seg2G A2

int count = 0;
int dezena = 0;
//int a = 0;
int counter = 0;

void printa();
void printaDezena();


ISR(TIMER2_OVF_vect){
  TCNT2 = 0;
  counter ++;
  if(counter == 5){
    printaDezena();
    printa();
    //as funcoes de printar
    //fazer isso a cada 30 msegundos
    counter = 0;
  }
}

void setup(){
  for(int i = 4;i <= 13;i ++){
    pinMode(i,OUTPUT);
  }
  pinMode(A2,OUTPUT);
  pinMode(A3,OUTPUT);
  pinMode(A4,OUTPUT);
  pinMode(A5,OUTPUT);

  //TIMER 1 COMPARACAO
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 0x3D09;
  TCCR1B |= (1<<WGM12)|(1<<CS10)|(1<<CS12);
  TIMSK1 |= (1<<OCIE1A);


  //TIMER 2 OVERFLOW
  TCCR2A = 0;
  TCCR2B = 7;//100
  TCNT2 = 0;
  TIMSK2 = 1;
}

/*ISR(TIMER2_OVF_vect){
  TCNT2 = 0;
  counter ++;
  if(counter == 100){
    printaDezena();
    printa();
    //as funcoes de printar
    //fazer isso a cada 30 msegundos
  }
}*/

//void printa();

ISR(TIMER1_COMPA_vect){
  Serial.println(TCNT1);
  Serial.println("-----INTERRUPCAO POR COMPARACAO-----");
  Serial.println(count);
  //printa();
  contador();
  /*if(count == 9){
    if(dezena == 9){
     dezena = 0;
    }
    else{
      dezena ++;
    }
  }
  
  if(count == 9){
    count = 0;
  }
  else{
    count ++;
  }*/
}

/*
ISR(TIMER2_OVF_vect){
  TCNT2 = 0;
  counter ++;
  if(counter == 50){
    printaDezena();
    printa();
    //as funcoes de printar
    //fazer isso a cada 30 msegundos
  }
}*/

void loop(){
  
}

void printa(){
  switch(count){
    case 0:{//0
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,HIGH);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,LOW);
      break;
    }
    case 1:{//1
      digitalWrite(seg2A,LOW);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,LOW);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,LOW);
      digitalWrite(seg2G,LOW);
      break;
    }
    case 2:{//2
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,LOW);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,HIGH);
      digitalWrite(seg2F,LOW);
      digitalWrite(seg2G,HIGH);   
      break;
    }
    case 3:{//3
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,LOW);
      digitalWrite(seg2G,HIGH);      
      break;
    }
    case 4:{//4
      digitalWrite(seg2A,LOW);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,LOW);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,HIGH);
      break;
    }
    case 5:{//5
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,LOW);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,HIGH);
      break;
    }
    case 6:{//6
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,LOW);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,HIGH);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,HIGH);
      break;
    }
    case 7:{//7
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,LOW);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,LOW);
      digitalWrite(seg2G,LOW);
      break;
    }
    case 8:{//8
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,HIGH);
      digitalWrite(seg2E,HIGH);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,HIGH);
      break;
    }
    case 9:{//9
      digitalWrite(seg2A,HIGH);
      digitalWrite(seg2B,HIGH);
      digitalWrite(seg2C,HIGH);
      digitalWrite(seg2D,LOW);
      digitalWrite(seg2E,LOW);
      digitalWrite(seg2F,HIGH);
      digitalWrite(seg2G,HIGH);
      //a = 1;
      break;
    }
  }
  //printaDezena();
}

void printaDezena(){
  switch(dezena){
    case 0:{//0
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,LOW);
      break;
    }
    case 1:{//1
      digitalWrite(segA,LOW);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,LOW);
      digitalWrite(segF,LOW);
      digitalWrite(segG,LOW);
      break;
    }
    case 2:{//2
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,LOW);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,LOW);
      digitalWrite(segG,HIGH);   
      break;
    }
    case 3:{//3
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,LOW);
      digitalWrite(segF,LOW);
      digitalWrite(segG,HIGH);      
      break;
    }
    case 4:{//4
      digitalWrite(segA,LOW);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,LOW);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 5:{//5
      digitalWrite(segA,HIGH);
      digitalWrite(segB,LOW);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,LOW);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 6:{//6
      digitalWrite(segA,HIGH);
      digitalWrite(segB,LOW);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 7:{//7
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,LOW);
      digitalWrite(segF,LOW);
      digitalWrite(segG,LOW);
      break;
    }
    case 8:{//8
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 9:{//9
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,LOW);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
  }
}

void contador(){
  if(count == 9){
    if(dezena == 9){
     dezena = 0;
    }
    else{
      dezena ++;
    }
  }
  
  if(count == 9){
    count = 0;
  }
  else{
    count ++;
  }
}


/*ISR(TIMER2_OVF_vect){
  TCNT2 = 0;
  counter ++;
  if(counter == 50){
    printaDezena();
    printa();
    //as funcoes de printar
    //fazer isso a cada 30 msegundos
  }
}*/

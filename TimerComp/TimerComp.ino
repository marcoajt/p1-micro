
/*#define segA 4
#define segB 5
#define segC 6
#define segD 7
#define segE 8
#define segF 9
#define segG 10*/



#define segA 9
#define segB 10
#define segC 4
#define segD 5
#define segE 6
#define segF 7
#define segG 8

int count = 0;

void setup(){
  Serial.begin(9600);
  for(int i = 4;i < 11;i ++){
    pinMode(i,OUTPUT);
  }
  //pinMode(2, INPUT);
  //pinMode(3, INPUT);
  attachInterrupt(0,avancar,RISING);
  attachInterrupt(1, voltar, RISING);


  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;
  OCR1A = 0xB71B;
  TCCR1B |= (1<<WGM12)|(1<<CS10)|(1<<CS12);
  TIMSK1 |= (1<<OCIE1A);
}

void voltar(){
  if(count == 0){
    count = 10;
  }
  else{
    count--;
  }
    //TCNT1 = 0;

  printa();
}

void avancar(){
  if(count == 10){
    count = 0;
  }
  else{
    count ++;
  }
  //TCNT1 = 0;

  printa();
}


void loop(){
  
}

ISR(TIMER1_COMPA_vect){
  Serial.println(TCNT1);
  Serial.println("-----INTERRUPCAO POR COMPARACAO-----");
  Serial.println(count);
  //digitalWrite();
  if(count == 10){
    count = 0;
  }
  else{
    count ++;
  }
  printa();
}


void  printa(){
  
  switch(count){
    case 0:{//H
      digitalWrite(segA,LOW);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 1:{//E
      digitalWrite(segA,HIGH);
      digitalWrite(segB,LOW);
      digitalWrite(segC,LOW);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 2:{//L
      digitalWrite(segA,LOW);
      digitalWrite(segB,LOW);
      digitalWrite(segC,LOW);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,LOW);      
      break;
    }
    case 3:{//L
      digitalWrite(segA,LOW);
      digitalWrite(segB,LOW);
      digitalWrite(segC,LOW);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,LOW);      
      break;
    }
    case 4:{//O
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,LOW);
      break;
    }
    case 5:{//A
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 6:{//T
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,LOW);
      digitalWrite(segE,LOW);
      digitalWrite(segF,LOW);
      digitalWrite(segG,LOW);
      break;
    }
    case 7:{//3
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,LOW);
      digitalWrite(segF,LOW);
      digitalWrite(segG,HIGH);
      break;
    }
    case 8:{//2
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,LOW);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,LOW);
      digitalWrite(segG,HIGH);
      break;
    }
    case 9:{//8
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,HIGH);
      digitalWrite(segD,HIGH);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
    case 10:{//P
      digitalWrite(segA,HIGH);
      digitalWrite(segB,HIGH);
      digitalWrite(segC,LOW);
      digitalWrite(segD,LOW);
      digitalWrite(segE,HIGH);
      digitalWrite(segF,HIGH);
      digitalWrite(segG,HIGH);
      break;
    }
  }
  }
